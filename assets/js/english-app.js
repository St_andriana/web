var subtitle =
  "Обери одну відповідь, яку вважаєш правильною та перевір свої знання";
var questions = [
  {
    text: "Яким словом можна привітатися?",
    answers: ["Hello", "Bye", "Ok"],
    correctAnswer: 0,
  },
  {
    text: "Good morning! - це",
    answers: ["Доброго вечора!", "Доброго дня!", "Доброго ранку!"],
    correctAnswer: 2,
  },
  {
    text: "Доброї ночі! - це",
    answers: ["Good afternoon!", "Good night!", "Hello!"],
    correctAnswer: 1,
  },
];

var yourAns = new Array();
var score = 0;

function Engine(question, answer) {
  yourAns[question] = answer;
}

function Score() {
  var answerText = "Результати:\n";
  for (var i = 0; i < yourAns.length; ++i) {
    var num = i + 1;
    answerText = answerText + "\n    Питання №" + num + "";
    if (yourAns[i] != questions[i].correctAnswer) {
      answerText =
        answerText +
        "\n    Правильна відповідь: " +
        questions[i].answers[questions[i].correctAnswer] +
        "\n";
    } else {
      answerText = answerText + ": Правильно! \n";
      ++score;
    }
  }

  answerText = answerText + "\nВсього правильних відповідей: " + score + "\n";

  alert(answerText);
  yourAns = [];
  score = 0;
  clearForm("quiz");
}

function clearForm(name) {
  var f = document.forms[name];
  for (var i = 0; i < f.elements.length; ++i) {
    if (f.elements[i].checked) f.elements[i].checked = false;
  }
}

let list = document.getElementById('answers_list');

for (var q = 0; q < questions.length; ++q) {
  var question = questions[q];
  var idx = 1 + q;

  list.innerHTML+='<li><span class="quest">' + question.text + "</span><br/>";

  for (var i in question.answers) {
    list.innerHTML+=
      '<input type=radio name="q' +
        idx +
        '" value="' +
        i +
        '" onClick="Engine(' +
        q +
        ', this.value)">' +
        question.answers[i] +
        "<br/>"
    ;
  }
}


